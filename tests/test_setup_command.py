from argparse import Namespace
from io import DEFAULT_BUFFER_SIZE
import subprocess
from os.path import exists
import unittest
import yaml
from yaml.loader import SafeLoader
from src.hpm.commands.SetupCommand import SetupCommand


class TestSetupCommand(unittest.TestCase):
    """Test setup command"""
    DEFAULT_TARGET = "https://gitedu.hesge.ch/api/v4"

    def setUp(self) -> None:
        self.args_full = Namespace(token="dummy_token", target=self.DEFAULT_TARGET)
        self.args_token_only = Namespace(token="dummy_token", target=None)
        self.command = SetupCommand("name", "help")

    def tearDown(self) -> None:
        subprocess.run(["rm", "./src/hpm/test_config.yaml"])

    def test_file_creation(self):
        self.command.execute(self.args_full, location="../test_config.yaml")

        self.assertTrue(exists("./src/hpm/test_config.yaml"))

    def test_token_content_equal_dummy_token(self):
        self.command.execute(self.args_full, location="../test_config.yaml")

        file = open("./src/hpm/test_config.yaml", "r")
        file_yaml = yaml.load(file, Loader=SafeLoader)
        
        self.assertEqual(file_yaml["token"], self.args_full.token)

    def test_target_content_equal_default_target(self):
        self.command.execute(self.args_token_only, location="../test_config.yaml")

        file = open("./src/hpm/test_config.yaml", "r")
        file_yaml = yaml.load(file, Loader=SafeLoader)
        
        self.assertEqual(file_yaml["target"], self.DEFAULT_TARGET)

    def test_target_update(self):
        self.command.execute(self.args_token_only, location="../test_config.yaml")

        previous = yaml.load(open("./src/hpm/test_config.yaml", "r"), Loader=SafeLoader)

        update_namespace = Namespace(token=None, target="https://gitlab.com/api/v4")
        self.command.execute(update_namespace, location="../test_config.yaml")

        current = yaml.load(open("./src/hpm/test_config.yaml", "r"), Loader=SafeLoader)
        self.assertNotEqual(previous["target"], current["target"])
