import unittest
from src.hpm._typing import Command, Parameter


class TestCommand(unittest.TestCase):
    """Test the abstract command class"""

    def test_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            c = Command(
                "name",
                "help",
                params=[Parameter(name="--token", help="Store API token")],
            )
            c.execute()
