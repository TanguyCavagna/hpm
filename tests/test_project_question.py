import re
import unittest
from ddt import ddt, data
from src.hpm._typing import ProjectQuestion


def external_def_validation(_input):
    return _input != ""


def external_def_transform(_input):
    return _input.upper()


@ddt
class TestProjectQuestion(unittest.TestCase):
    """Test the project question"""

    def test_init_raise_type_error(self):
        with self.assertRaises(TypeError):
            q = ProjectQuestion()

    def test_post_init_raise_value_error(self):
        with self.assertRaises(ValueError):
            q = ProjectQuestion(message="", name="", default="", help="")

    # Data structure : (<validation method>, <provided data>, <excpected output>)
    @data(
        (lambda v: v != "", "", False),
        (lambda v: v != "", "John Doe", True),
        (lambda v: len(v) >= 2, "", False),
        (lambda v: len(v) >= 2, "John Doe", True),
        (lambda v: external_def_validation(v), "", False),
        (lambda v: external_def_validation(v), "john doe", True),
        (lambda v: bool(re.search(r"^[\w\d\-]*$", v)), "john-doe", True),
        (lambda v: bool(re.search(r"^[\w\d\-]*$", v)), "john-doe2", True),
        (lambda v: bool(re.search(r"^[\w\d\-]*$", v)), "john doe 2", False),
    )
    def test_validate_response(self, validate):
        q = ProjectQuestion(
            message="Validate",
            name="validate",
            default="",
            help="",
            validate=validate[0],
        )
        q.set_response(validate[1])
        self.assertEqual(validate[2], q.validate(q.response))

    # Data structure : (<transformation method>, <provided data>, <excpected output>)
    @data(
        (lambda v: v.upper(), "john doe", "JOHN DOE"),
        (lambda v: v.replace(" ", "-"), "john doe", "john-doe"),
        (lambda v: v.replace(" ", "-").upper(), "john doe", "JOHN-DOE"),
        (lambda v: external_def_transform(v), "john doe", "JOHN DOE")
    )
    def test_response_transform(self, transform):
        q = ProjectQuestion(
            message="Validate",
            name="validate",
            default="",
            help="",
            transform=transform[0],
        )
        q.set_response(transform[1])
        q.transform_response(q.response)
        self.assertEqual(q.response, transform[2])
