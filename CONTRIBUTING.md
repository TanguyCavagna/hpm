# Contributing to **hpm**

👍🎉 First off, thanks for taking the time to contribute ! 🎉👍

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.
Please note we are going to add a [code of conduct](), please follow it in all your interactions with the project when it's ready.

## Table of contents

- [Setting Up the project locally](#setting-up-the-project-locally)
- [Submitting a Merge Request](#submitting-a-merge-request)
- [Add yourself as a contributor](#add-yourself-as-a-contributor)

## Setting Up the project locally

To install the project you need to have `python`

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the project, clone your fork :
    ```bash
    # Clone your fork
    git clone https://gitedu.hesge.ch/<your-username>/hpm.git

    # Navigate to the newly clonned directory
    cd hpm
    ```
2. Your environment needs to be running `python` version >= 3.8, and have the following depedencies installed :
    - `requests`
    - `rich`
3. To run the project locally :
    ```bash
    # While being at the root of the project
    python -m src.hpm <command>
    ```

## Submitting a Merge Request

Please go through existing issues and merge requests to check if somebody else is already working on it.

Also, make sure to format your code using the python `black` formatter.
```bash
black {source_file_or_directory}

# If running as a package is not working
python -m black {source_file_or_directory}
```

## Add yourself as a contributor

To add yourself to the table of contributors on the `REAMDME.md` :

```md
## Contributors ✨

* ...
* @<your-username>
* ...
```

**⚠️Do not forget to add the `README.md` to your MR⚠️**