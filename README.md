# Welcome to **hpm** 👋

> hpm - an HEPIA project manager

![banner](https://i.imgur.com/GAMyEle.png)

### [🗃️ Package](https://pypi.org/project/hpm)

## Install

```
pip install hpm
```

## Author

👤 **Tanguy Cavagna <tanguy.cavagna@etu.hesge.ch>**
- Gitlab: @tanguy.cavagna

## 🤝 Contributing

Contributions, issues and merge requests are welcome!
Feel free to check [issues page](https://gitedu.hesge.ch/tanguy.cavagna/hpm/-/issues).

When doing a MR, please follow this title naming convention :

- Your title must be clear and self-documented
- It can starts with one of those emoji if you want :
  - ✨ for introducing a new feature
  - 🎨 for structure/format of the code improvement
  - ⚡ for performance improvement
  - 🔥 for code/file deletion
  - 🐛 for any bug correction
  - 🚑 for a critical hotfix
  - 📝 for documentation add/update
  - 🚀 for deployement
  - 💄 for UI improvement
  - 🔧 for configuration changes
  - ✏️ for typos
  
  > If the emoji is not listed here, refer to [https://gitmoji.dev/](https://gitmoji.dev/) to use the correct one.

## Show your support

Give a ⭐ if you like this project or if it helped you !

## 📝 License

Copyright © 2021 Tanguy Cavagna
This project is [MIT](https://gitedu.hesge.ch/tanguy.cavagna/hpm/-/blob/master/LICENSE) licensed.

## Contributors ✨ 

_Contribute to this project and you will be added to this section !_
