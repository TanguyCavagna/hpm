from . import hpm

# To run the package as a standalone command
def main():
    hpm.main()


if __name__ == "__main__":
    main()
